FROM hashicorp/terraform:0.12.13 as terraform
FROM python:3.7-slim

RUN apt-get update && \
    apt install make -y && \
    rm -rf /var/lib/apt/lists/*

COPY --from=terraform /bin/terraform /bin/terraform

RUN pip install awscli
